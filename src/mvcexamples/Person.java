/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvcexamples;

/**
 *
 * @author dannylowpass
 */
public class Person {
    private String name;
    private double height;
    private double weight;
    Weight unitWeight;
    Height unitHeight;
            
    public enum Weight{
        KG, LB
    }
    
    public enum Height{
        M, IN
    }
    
    public enum UnitsOfMeasure{
        KG,LB, M, IN
    }
    
    public Person(){
        
    }
    
    public Person(String name, double height, double weight){
        this.name = name;
        this.weight = weight;
        this.height = height;
    }
    
    public String getName(){
        return name;
    }
    
    public double getWeight(){
        return weight;
    }
    
    public double getHeight(){
       return height;
   }
   
    public void setName(String newName){
       this.name = newName;
   }
    
    public void setWeight(double newWeight){
       this.weight = newWeight;
   }
    
    public void setheight(double newheight){
       this.weight = newheight;
   }
    
    public double getBMI(){
        
        double weightInKilos = 0;
        double heightInMetres = 0;
        /*
        if(unitWeight == Weight.k && unitHeight == Height.m){
            return weight/(Math.pow(height, 2));
        }*/
        
        switch (unitWeight){
            case KG : weightInKilos = weight;
            break;
            case LB : weightInKilos = weight / 0.4535;
        }
        
        switch (unitHeight){
            case M : heightInMetres = weight;
            break;
            case IN : heightInMetres = weight * 2.205;
        }return weight;
    }
    
}
